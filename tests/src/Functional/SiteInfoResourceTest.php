<?php

namespace Drupal\Tests\site_info_rest\Functional;

use Drupal\Tests\BrowserTestBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tests for site info rest endpoints.
 *
 * @group site_info_rest
 */
class SiteInfoResourceTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'system',
    'site_info_rest',
    'site_info_rest_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set a site slogan and name.
    $this->config('system.site')
      ->set('slogan', 'Test resource slogan')
      ->set('name', 'Test resource')
      ->save();
    $this->config('stark.settings')
      ->set('logo.path', 'public://test.png')
      ->set('favicon.path', 'public://favicon.ico')
      ->save();
  }

  /**
   * Test method GET site info resources.
   */
  public function testSiteInfoRestGetInformation() {
    $this->drupalLogin($this->rootUser);
    $logo = theme_get_setting('logo.url');
    $favicon = theme_get_setting('favicon.url');
    $site_url = \Drupal::requestStack()->getCurrentRequest()->getSchemeAndHttpHost();
    $logo_response = '"logo":' . '"' . str_replace('/', '\/', $site_url . $logo) . '"';
    $favicon_response = '"favicon":' . '"' . str_replace('/', '\/', $site_url . $favicon) . '"';

    $this->drupalGet("/site/info", ['query' => ['_format' => 'json']]);
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseContains('"name":"Test resource"');
    $this->assertSession()->responseContains('"slogan":"Test resource slogan"');
    $this->assertSession()->responseContains($logo_response);
    $this->assertSession()->responseContains($favicon_response);
  }

}
