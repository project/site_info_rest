<?php

namespace Drupal\site_info_rest\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Represents site information as resource.
 *
 * @RestResource(
 *   id = "site_info",
 *   label = @Translation("Site Information"),
 *   uri_paths = {
 *     "canonical" = "/site/info"
 *   }
 * )
 */
class SiteInfo extends ResourceBase {

  /**
   * The system.site config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $siteConfig;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a SiteInfo object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, ConfigFactoryInterface $config_factory, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->siteConfig = $config_factory->get('system.site');
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * Responds to site information GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   A resource response containing information about the site, including
   *   its name, slogan, logo, and favicon.
   */
  public function get(): ResourceResponse {
    // @todo Replace when the issue is fixed: https://www.drupal.org/project/drupal/issues/3035288
    $logo = theme_get_setting('logo.url');
    $favicon = theme_get_setting('favicon.url');
    $site_url = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $response = [
      'favicon' => $favicon ? $site_url . $favicon : NULL,
      'logo' => $logo ? $site_url . $logo : NULL,
      'name' => $this->siteConfig->get('name'),
      'slogan' => $this->siteConfig->get('slogan'),
    ];
    return new ResourceResponse($response);
  }

}
