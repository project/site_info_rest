CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Get basic site information via REST(logo, favicon, site name and site slogan).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/site_info_rest

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/site_info_rest

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/extending-drupal/installing-modules
   for further information.



USAGE
-------------
1. Enable the module.
2. Retrieve Site Information via REST using GET query to /site/info?_format=json.

CONFIGURATION
-------------
Use [REST UI](https://www.drupal.org/project/restui) module for configuration this rest resource.


MAINTAINERS
-----------

Current maintainers:
 * Pablo Pukha (Pablo_Pukha) - https://www.drupal.org/u/Pablo_Pukha
